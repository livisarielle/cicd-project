import subprocess
from flask import Flask

app = Flask(__name__)

@app.route('/webhook', methods=['POST'])
def webhook():
    # Récupérer la liste de tous les conteneurs à gérer
    containers_to_manage = subprocess.check_output(
        ["docker", "ps", "-a", "-q", "--filter", "label=manage_by_script=yes"]
    ).decode().split()

    # Arrêter et supprimer les conteneurs sélectionnés
    if containers_to_manage:
        subprocess.run(["docker", "stop"] + containers_to_manage)
        subprocess.run(["docker", "rm"] + containers_to_manage)

    # Supprimer toutes les images Docker liées à rainette03/pipelinecicd:latest
    images_to_remove = subprocess.check_output(
        ["docker", "images", "rainette03/pipelinecicd:latest", "-q"]
    ).decode().split()
    if images_to_remove:
        subprocess.run(["docker", "rmi"] + images_to_remove)

    # Tirer la dernière image Docker avec le tag :latest et relancer le conteneur
    subprocess.run(["docker", "pull", "rainette03/pipelinecicd:latest"])
    subprocess.run(["docker", "run", "-d", "-p", "8080:80", "rainette03/pipelinecicd:latest"])

    print("Webhook reçu et conteneur mis à jour.")
    return "Webhook traité", 200

if __name__ == '__main__':
    app.run(debug=True, port=5000)
