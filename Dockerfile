#Utiliser Nginx comme serveur web
FROM nginx:alpine

#Copier uniquement les fichiers nécessaires dans le conteneur
COPY index.html /usr/share/nginx/html
COPY style.css /usr/share/nginx/html

#Exposer le port 80
EXPOSE 80

#Lancer Nginx
CMD ["nginx", "-g", "daemon off;"]